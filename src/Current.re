type state = {
  count: int,
  isOpen: bool,
  lastChecked: int,
};

let initialState = {
  count: 0,
  isOpen: true,
  lastChecked: 0,
};

module Decode = {
  // the server encodes everything as a string so we have to do
  // a double step conversion and the type must be `string`
  let info = json => {
    Json.Decode.{
      count: json |> field("current", string) |> int_of_string,
      lastChecked: json |> field("time", string) |> int_of_string,
      isOpen: json |> _ => true,
    };
  };
};

module Info = {
  // 
  let fetch = () => {
    Fetch.fetch("https://publiek.usc.ru.nl/app/api/v1/?module=other&method=getFitnessInfo&lang=en")
    |> Js.Promise.then_(Fetch.Response.json)
  }
};

[@react.component]
let make = () => {
  let (state, setState) = React.useState(() => initialState);

  React.useEffect0(() => {
    Info.fetch()
    |> Js.Promise.then_(json => {
      setState(_ => Decode.info(json));
      Js.Promise.resolve();
    });
    None;
  });

  <div>
  {
    switch (state) {
      | {count: _, isOpen: false } => React.string("Gym closed")
      | {count: n, isOpen: true } => React.string("Present: " ++ string_of_int(state.count))
    }
  }
  </div>
};
